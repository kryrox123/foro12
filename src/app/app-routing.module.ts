import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LandingComponent } from './components/landing/landing.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { canActivate,redirectUnauthorizedTo,redirectLoggedInTo } from '@angular/fire/auth-guard';
import { PublicacionesComponent } from './components/home/publicaciones/publicaciones.component';
import { InicioComponent } from './components/home/inicio/inicio.component';
import { AgregarPubliComponent } from './components/home/agregar-publi/agregar-publi.component';


const redirectToLogin = () =>redirectUnauthorizedTo(['login']);
const redirectToHome = () =>redirectLoggedInTo(['home']);

const routes: Routes = [
  {path:'',pathMatch:'full',component:LandingComponent},
  {path:'login',
  component:LoginComponent, 
  ...canActivate(redirectToHome)
},
  {path:'sign-up',
  component:SignUpComponent,
  ...canActivate(redirectToHome)
},
  {
    path:'home',
    component:HomeComponent, 
    ...canActivate(redirectToLogin),
    children:[
      {path:'publicaciones',component:PublicacionesComponent},
      {path:'inicio',component:InicioComponent},
      
    ]
  },
  {path:'agregar-publicacion',component:AgregarPubliComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
