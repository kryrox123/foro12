import { Injectable } from '@angular/core';
import { IPublicacion } from '../models/publicacion.interface';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {
  post:IPublicacion[]=[];
  constructor() {
    this.post=[
      {titulo:'Phantom',fecha:new Date(),descripcion:'Ya todo listo para el día de combate, esperemos que todo salga bien'},

    ];
  }

  obtenerPost(){
    return this.post;
  }

  addPost(publicacion:IPublicacion){
    this.post.push(publicacion);
    return false;
  }
}
